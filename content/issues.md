+++
title = "Issues"
description = "A list of interesting issues"
+++

Last update: 2022-12-21

# Element Android
- <https://github.com/vector-im/element-android/issues/3417>: No avatars or displaynames when scrolling up (Conduit)
- <https://github.com/vector-im/element-android/issues/1924>: UIAA for register is wrong (needs MSC)

# Element Web
- <https://github.com/vector-im/element-web/issues/22565>: Element puts rooms into archived section
- <https://github.com/vector-im/element-web/issues/21034>: Missing avatars or displaynames when scrolling up (Conduit)
- <https://github.com/vector-im/element-web/issues/17397>: Element shows room version warning when joining a room (Conduit)
- <https://github.com/vector-im/element-web/issues/20550>: Alt text for user avatars is unnecessary

# Element iOS
- <https://github.com/vector-im/element-ios/issues/5974> User verification does not work on Conduit
- <https://github.com/vector-im/element-ios/issues/5538> Account deactivation doesn't work on Conduit servers

# Matrix Spec
- <https://github.com/matrix-org/matrix-spec/issues/213>: Make peeking not use deprecated endpoints (/initialSync)
- <https://github.com/matrix-org/matrix-spec/issues/942>: lazy-loading behaviour for /sync is incorrectly specified
- <https://github.com/matrix-org/matrix-spec/issues/804>: What exactly should servers do when they receive events over federation
- <https://github.com/matrix-org/matrix-spec/issues/659> PUT /rooms/{roomId}/redact/{eventId}/{txnId} makes no sense
- <https://github.com/matrix-org/matrix-spec/issues/852>: Event ordering
- <https://github.com/matrix-org/matrix-spec/issues/732>: Clarify UI authentication behavior around resubmitting the original request
- <https://github.com/matrix-org/matrix-spec/issues/686>: Filters are confusing
- <https://github.com/matrix-org/matrix-spec/issues/803>: Specify what exactly happens to events that fail signature verification
- <https://github.com/matrix-org/matrix-spec-proposals/issues/3764> User directory definition of "public room"
- <https://github.com/matrix-org/matrix-spec/issues/705>: /send's pdus field should be optional
- <https://github.com/matrix-org/matrix-spec/issues/801>: Should invite_state contain the invite event?
- <https://github.com/matrix-org/matrix-spec/issues/964>: /createRoom is not clear about what should happen if you specify yourself as an invitee
- <https://github.com/matrix-org/matrix-spec/issues/374>: Make transaction.origin and pdu.origin optional/go away
- <https://github.com/matrix-org/matrix-spec/issues/674>: /profile body parameters: required not omittable
- <https://github.com/matrix-org/matrix-spec/issues/813>: Unclear what /user/devices should return when user or keys are not found
- <https://github.com/matrix-org/matrix-spec/issues/626>: Decide which Content-Disposition header the media repo serves
- <https://github.com/matrix-org/matrix-spec-proposals/pull/2848>: Globally unique event IDs
- <https://github.com/matrix-org/matrix-spec-proposals/pull/2448>: Using BlurHash as a Placeholder for Matrix Media
- <https://github.com/matrix-org/matrix-spec-proposals/pull/2828>: Proposal to restrict allowed user IDs over federation
- <https://github.com/matrix-org/matrix-spec-proposals/pull/3214>: Allow overriding m.room.power_levels using initial_state
- <https://github.com/matrix-org/matrix-spec-proposals/pull/3618>: Add proposal to simplify federation /send response

# Ruma
- <https://github.com/ruma/ruma/issues/612>: Allow streaming binary http bodies
- <https://github.com/ruma/ruma/issues/839>: Key identifier error on matrix.org should be allowed with compat
- <https://github.com/ruma/ruma/issues/858>: Unnecessary fetch_events
- <https://github.com/ruma/ruma/issues/164>: Validate size in bytes of certain types according to spec
- <https://github.com/ruma/ruma/issues/447>: Add check to make sure there are <= 20 prev events on pdus
- <https://github.com/ruma/ruma/issues/507>: serde_json::error::RecursionLimitExceeded exists

# Synapse
- <https://github.com/matrix-org/synapse/issues/5393>: synapse always includes redundant m.room.member events for most endpoints with lazy-loading enabled
- <https://github.com/matrix-org/synapse/issues/1563>: unbans do not propagate to federated servers
- <https://github.com/matrix-org/synapse/issues/11731>: Synapse does not respect event order of spec /createRoom
- <https://github.com/matrix-org/synapse/issues/3816>: transaction.origin and pdu.origin are redundant and we should remove them
- <https://github.com/matrix-org/synapse/issues/8307>: Synapse allows invalid characters in the signature key id
- <https://github.com/matrix-org/synapse/issues/9595>: rejected event used as an auth event
- <https://github.com/matrix-org/synapse/issues/9799>: synapse accepted an event with no prev_events
- <https://github.com/matrix-org/synapse/issues/10715>: A m.room.power_levels state event was accepted with newlines in user IDs
- <https://github.com/matrix-org/synapse/issues/8334>: Investigate if there are unspecced endpoints that can be removed
- <https://github.com/matrix-org/synapse/issues/8917>: we should be more intelligent with backoff for federation requests
- <https://github.com/matrix-org/synapse/issues/10519>: Accepts invalid room tags
- <https://github.com/matrix-org/synapse/issues/10304>: Wrong type for /publicRooms limit

# Hydrogen

- <https://github.com/vector-im/hydrogen-web/issues/741>: PUT matrix/client/r0/room_keys/keys is used, while it was never specced

# Cinny

- <https://github.com/ajbura/cinny/issues/581>: Certain rooms not showing when using Conduit server

# Fluffychat

- <https://gitlab.com/famedly/fluffychat/-/issues/691>: Stuck in "Synchronizing... Please Wait." or "Loading... Please Wait."

# Sytest
- <https://github.com/matrix-org/sytest/issues/919>: Sytest uses non mxc url for avatar
- <https://github.com/matrix-org/sytest/issues/1069>: Unknown "GET /rooms/:room_id/state/m.room.member/:user_id?format=event" API

# Dendrite
- <https://github.com/matrix-org/dendrite/issues/1997>: Dendrite cannot load conduit media

# Matrix-Appservice-Discord 
- <https://github.com/Half-Shot/matrix-appservice-discord/pull/680>: fix: use default permissions when not set explicitly

# Matrix-ircd
- <https://github.com/matrix-org/matrix-ircd/issues/68>: client/r0/login lacks required field "identifier"

# RocksDB
- <https://github.com/rust-rocksdb/rust-rocksdb/issues/587>: Support Write Buffer Manager

# Mirage
- <https://github.com/mirukana/mirage/issues/252>: Login doesn't work with conduit


# Done
- <https://github.com/vector-im/element-android/issues/5166>: Repeated timeline, many read receipts on Conduit
- <https://github.com/vector-im/element-web/issues/21669>: Element depends on optional power level fields to show "change powerlevel" UI
- <https://github.com/vector-im/element-android/issues/4830>: Use stable version of the room alias endpoints instead of MSC2432
- <https://github.com/vector-im/element-android/issues/5512>: Formatting should not work in code blocks
- <https://github.com/vector-im/element-web/issues/21680>: Element Web calls don't work if /thirdparty/user/im.vector.protocol.sip_virtual is not implemented
- <https://github.com/vector-im/element-web/issues/14046>: Direct Message room detection logic not in sync with the spec
- <https://github.com/vector-im/element-web/issues/17263>: EDUs with room id field cause notifications
- <https://github.com/matrix-org/matrix-spec/issues/860>: Default invite power-level is incorrect
- <https://github.com/matrix-org/matrix-spec/issues/817>: Exact Federation Authorization header format isn't clear 
- <https://github.com/matrix-org/matrix-spec/issues/645>: PushRule endpoint has wrong description
- <https://github.com/ruma/ruma/issues/818>: Support M_UNABLE_TO_AUTHORISE_JOIN
- <https://github.com/seanmonstar/reqwest/pull/1441>: Dns overrides with a function
