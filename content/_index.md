+++
title = "Conduit"
sort_by = "weight"
+++

> Note: This project is beta. It can be used already, but is missing some smaller features.

# What is Matrix?

[Matrix](https://matrix.org) is an open network for secure and decentralized
communication. Users from every Matrix homeserver can chat with users from all
other Matrix servers. You can even use bridges (also called Matrix appservices) to
communicate with users outside of Matrix, like a community on Discord.

# Why Conduit?

Conduit is a lightweight open-source server implementation of the [Matrix
Specification](https://matrix.org/docs/spec) with a focus on easy setup and low
system requirements. That means you can make your own Conduit setup in just
a few minutes.

Other server implementations try to be extremely scalable, which makes sense if
the goal is to support millions of users on a single instance, but makes
smaller deployments inefficient. Conduit tries to keep it simple. The future
for Conduit in peer-to-peer Matrix (every client contains a server) is also
bright.

# Links

Website: <https://conduit.rs>\
Git and Documentation: <https://gitlab.com/famedly/conduit>\
Chat with us: [#conduit:fachschaften.org](https://matrix.to/#/#conduit:fachschaften.org)

# Donate

Liberapay: <https://liberapay.com/timokoesters/>\
Bitcoin: `bc1qnnykf986tw49ur7wx9rpw2tevpsztvar5x8w4n`

Server hosting for `conduit.rs` donated by the Matrix.org Foundation.


<img style="width:50%" src="BMBF_gefoerdert_2017_en.jpg">
Sponsored for 6 months in 2021. FKZ: 01lS21S11
