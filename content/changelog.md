+++
title = "Changelog"
description = "Changelog"
weight = 2
+++

To update conduit, simply stop it, replace the binary and start it again.

# v0.5.0 - 2022-12-21

- Feature: Restricted room joining [!398](https://gitlab.com/famedly/conduit/-/merge_requests/398)
- Feature: Call sd-notify after init and before exit [!426](https://gitlab.com/famedly/conduit/-/merge_requests/426)
- Improvement: V9 as default room version [!400](https://gitlab.com/famedly/conduit/-/merge_requests/400)
- Improvement: More efficient E2EE key claiming [!389](https://gitlab.com/famedly/conduit/-/merge_requests/389)
- Fix: All E2EE problems [!393](https://gitlab.com/famedly/conduit/-/merge_requests/393)
- Fix: Infinite room loading [!388](https://gitlab.com/famedly/conduit/-/merge_requests/388)
- Fix: Wrong notification rules [!405](https://gitlab.com/famedly/conduit/-/merge_requests/405)
- Fix: Wrong notification counts [!408](https://gitlab.com/famedly/conduit/-/merge_requests/408)
- Fix: Can't rejoin rooms [!399](https://gitlab.com/famedly/conduit/-/merge_requests/399)
- Fix: Fluffychat login works again [!391](https://gitlab.com/famedly/conduit/-/merge_requests/391)
- Fix: Starting DMs with Synapse users [!390](https://gitlab.com/famedly/conduit/-/merge_requests/390)
- Fix: is_guest for appservices [!401](https://gitlab.com/famedly/conduit/-/merge_requests/401)
- Fix: Invites from Dendrite [!416](https://gitlab.com/famedly/conduit/-/merge_requests/416)
- Fix: Send unrecognized error for unknown endpoints [!397](https://gitlab.com/famedly/conduit/-/merge_requests/397)
- Refactor: Service layer [!365](https://gitlab.com/famedly/conduit/-/merge_requests/365)

# v0.4.0 - 2022-06-23

- Breaking: Docker user changed. If you use Docker, please chown your database directory to `1000:1000`
- Feature: Conduit now supports room versions 3 to 9 [!257](https://gitlab.com/famedly/conduit/-/merge_requests/257)
- Feature: Deactivate user command [!337](https://gitlab.com/famedly/conduit/-/merge_requests/337)
- Feature: Admin command to create new user [!354](https://gitlab.com/famedly/conduit/-/merge_requests/354)
- Feature: Admin command to show config [!295](https://gitlab.com/famedly/conduit/-/merge_requests/295)
- Feature: Admin command to change password [!354](https://gitlab.com/famedly/conduit/-/merge_requests/354)
- Feature: Config option for emergency password to rescue server [!354](https://gitlab.com/famedly/conduit/-/merge_requests/354)
- Feature: Sync performance improved [!297](https://gitlab.com/famedly/conduit/-/merge_requests/297)
- Feature: Lightning bolt optional [!366](https://gitlab.com/famedly/conduit/-/merge_requests/366)
- Improvement: Jemalloc can now be disabled at compile time [!285](https://gitlab.com/famedly/conduit/-/merge_requests/285)
- Improvement: Use native CA certificates [!329](https://gitlab.com/famedly/conduit/-/merge_requests/329)
- Improvement: Case insensitive userid login [!323](https://gitlab.com/famedly/conduit/-/merge_requests/323)
- Fix: Element Android notifications should work again
- Fix: Fluffychat compatibility [!293](https://gitlab.com/famedly/conduit/-/merge_requests/354)
- Fix: Kick and ban events over federation [!338](https://gitlab.com/famedly/conduit/-/merge_requests/338)
- Fix: Editing state works again [!336](https://gitlab.com/famedly/conduit/-/merge_requests/336)
- Fix: Admin bot can no longer trigger itself [!293](https://gitlab.com/famedly/conduit/-/merge_requests/293)
- Fix: Appservice compatibility [!331](https://gitlab.com/famedly/conduit/-/merge_requests/331)
- Fix: Redacting unknown events will no longer cause problems [!298](https://gitlab.com/famedly/conduit/-/merge_requests/298)
- Fix: Admin command parse-pdu no longer crashes for malformed jsons [!304](https://gitlab.com/famedly/conduit/-/merge_requests/304)
- Fix: Hide users from user directory if they are only in private rooms and they don't share a room [!325](https://gitlab.com/famedly/conduit/-/merge_requests/325)
- Fix: Don't panic when signing event fails [!343](https://gitlab.com/famedly/conduit/-/merge_requests/343)
- Docs: TURN server guide [!291](https://gitlab.com/famedly/conduit/-/merge_requests/291)
- Docs: Admin room welcome message mentions `help` command [!299](https://gitlab.com/famedly/conduit/-/merge_requests/299)
- Docs: Change setup guides to use the same database paths [!301](https://gitlab.com/famedly/conduit/-/merge_requests/301)
- Docs: Clarify that Conduit is in beta [!310](https://gitlab.com/famedly/conduit/-/merge_requests/310)
- Docs: Fix proxy examples [!321](https://gitlab.com/famedly/conduit/-/merge_requests/321)
- Refactor: Changed from rocket to axum [!263](https://gitlab.com/famedly/conduit/-/merge_requests/263)

# v0.3.0 - 2022-02-04

- Feature: Support server ACLs [!248](https://gitlab.com/famedly/conduit/-/merge_requests/248)
- Feature: RocksDB Database Backend [!217](https://gitlab.com/famedly/conduit/-/merge_requests/217)
- Feature: Database backend selection at runtime [!217](https://gitlab.com/famedly/conduit/-/merge_requests/217)
- Feature: Report users to homeserver admin [!218](https://gitlab.com/famedly/conduit/-/merge_requests/218)
- Feature: Creation of Spaces (exploring spaces is not supported yet) [!220](https://gitlab.com/famedly/conduit/-/merge_requests/220)
- Feature: Enable voice calls (requires configuring a TURN server) [!208](https://gitlab.com/famedly/conduit/-/merge_requests/208)
- Feature: Lazy loading for much faster initial syncs [!240](https://gitlab.com/famedly/conduit/-/merge_requests/240)
- Improvement: Batch inserts for events [!204](https://gitlab.com/famedly/conduit/-/merge_requests/204)
- Improvement: Significantly faster state resolution [!217](https://gitlab.com/famedly/conduit/-/merge_requests/217)
- Improvement: Reuse reqwest client [!265](https://gitlab.com/famedly/conduit/-/merge_requests/265)
- Docs: Better appservice instructions [!196](https://gitlab.com/famedly/conduit/-/merge_requests/196)
- Fix: Very old events appear in the timeline [!188](https://gitlab.com/famedly/conduit/-/merge_requests/188)
- Fix: Sync not waking up on new events in room [!194](https://gitlab.com/famedly/conduit/-/merge_requests/194)
- Fix: Crash on empty search [!286](https://gitlab.com/famedly/conduit/-/merge_requests/286)
- Fix: Better E2EE support
- Fix: Stack overflows

# v0.2.0 - 2021-09-01

- Initial release
- Almost all client server and server server features of the Matrix Spec are implemented
- Notable exceptions: User verification over federation, room versions > 6

# v0.0.0 - 2020-02-15

- Start of development
